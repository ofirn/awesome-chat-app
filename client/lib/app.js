angular.module('chat', [
	'angular-meteor',
	'ui.router',
	'ngMaterial',
	'luegg.directives'
	]);

Accounts.ui.config({
	passwordSignupFields: "USERNAME_ONLY"
});

function onReady() {
	angular.bootstrap(document, ['chat'], {
		strictDi: true
	});
}

angular.element(document).ready(onReady);
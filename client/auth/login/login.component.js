angular.module('chat').directive('login', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/auth/login/login.html',
		controllerAs: 'login',
		controller: function ($scope, $reactive, $state) {
			$reactive(this).attach($scope);

			this.username = '';
			this.password = '';
			this.error = '';

			this.login = () => {
				Meteor.loginWithPassword(
					this.username,
					this.password,
					(err) => {
						if (err) {
							this.error = err;
						}
						else{
							$state.go('room');
						}
					});
				};
			}
		}
});
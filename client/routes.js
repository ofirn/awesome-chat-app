angular.module('chat')
.config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
		// rely on history.pushState to change urls where supported
		$locationProvider.html5Mode(true); 

		$stateProvider
		// Login
		.state('login', {
			url: '/login',
			template: '<login></login>'
		})
		// Register
		.state('register', {
			url: '/register',
			template: '<register></register>'
		})
		.state('room', {
			url: '/room',
			template: '<room></room>',
			resolve: {
				// Allow to move to Chat only if the user is logged in,
				// Otherwise go to Login
				currentUser: ($q) => {
					if (Meteor.userId() == null){
						return $q.reject('AUTH_REQUIRED');
					}
					else{
						return $q.resolve();	
					}					
				}
			}
		});

		$urlRouterProvider.otherwise('/room');
	})

.run(function ($rootScope, $state) {
	$rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
		if (error === 'AUTH_REQUIRED') {
			$state.go('login');
		}
	});
});
angular.module('chat').directive('register', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/auth/register/register.html',
		controllerAs: 'register',
		controller: function ($scope, $reactive, $state) {
			$reactive(this).attach($scope);

			this.username = '';
			this.password = '';
			this.error = '';

			this.register = () => {
				Accounts.createUser(
					{
						username: this.username,
						password: this.password
					},
					(err) => {
						if (err) {
							this.error = err;
						}
						else{
							$state.go('room');
						}
					});
				};
			}
		}
});
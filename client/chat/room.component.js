angular.module('chat').directive('room', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/chat/room.html',
		controllerAs: 'room',
		controller: function ($scope, $reactive, $state) {
			$reactive(this).attach($scope);

			this.subscribe('messages');
			this.newMessage = {};

			this.helpers({
				messages: () => {
					return Messages.find({});
				}
			});

			this.addMessage = () => {
				// add the user id
				this.newMessage.user = Meteor.user().username;
				// add the current time
				var date = new Date();
				this.newMessage.time = date.toString(); 
				// add the message
				Messages.insert(this.newMessage);
				this.newMessage = {};
			};
			}
		}
	});
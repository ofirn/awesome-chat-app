Messages = new Mongo.Collection("messages");

Messages.allow({
	// Remove function is not allowed
	remove: function (userId, doc) {
		return false;
	},
	// Update function is not allowed
	update: function (userId, doc) {
		return false;
	},
	// User has to be logged-in in order to insert new doc
	insert:  function (userId, doc) {
		return userId !== null ? true : false;
	}
})